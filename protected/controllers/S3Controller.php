<?php

/*
 * S3 examples to create an object and to get an object from S3
 */

class S3Controller extends Controller {

    public function actionCreateObject() {
        Yii::import('application.vendors.*');
        require_once('AWS/sdk.class.php');
        $s3 = new AmazonS3();
        $bucketname = "yiiaws";
        $filename = "MyFileName";

        $response = $s3->create_object($bucketname, $filename, array(
            'body' => "EMPTY",
            'contentType' => 'text/plain',
            'acl' => $s3::ACL_PUBLIC
                ));

        /*
         * If the status is 200 is because everything went right.
         */

        if ($response->status == 200) {
            Yii::app()->user->setFlash('success', "$filename was created in $bucketname");
            $url = $response->header['x-aws-request-url'];
            $this->render('createObject', array(
                'url' => $url
            ));
        } else {

            /*
             * There are a lot of possible errors. The idea is to get them later.
             */

            Yii::app()->user->setFlash('error', $response->status . " : " . $response->body->Message);
            $this->render('createObject', array(
            ));
        }
        //print_r($response);
    }

    public function actionGetObject() {
        Yii::import('application.vendors.*');
        require_once('AWS/sdk.class.php');
        $s3 = new AmazonS3();
        $bucketname = "yiiaws";
        $filename = "MyFileName";
        $where = sys_get_temp_dir() . "/" . $filename;


        $response = $s3->get_object($bucketname, $filename, array(
            'fileDownload' => $where
                ));

        if ($response->status == 200) {
            Yii::app()->user->setFlash('success', "$filename was downloaded from $bucketname to <b><i>$where</b></i>");
        } else {
            Yii::app()->user->setFlash('error', "Error : ".$response->status);
        }
        $this->render('getObject', array(
        ));
    }

    public function actionIndex() {
        $this->render('index');
    }

}