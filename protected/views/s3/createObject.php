<?php
/* @var $this S3Controller */

$this->breadcrumbs=array(
	'S3'=>array('/s3'),
	'CreateObject',
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
?>
<p>

<?
if(isset($url))
    echo CHtml::link("View File", $url);
?>
    
</p>
