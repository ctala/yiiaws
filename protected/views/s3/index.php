<?php
/* @var $this S3Controller */

$this->breadcrumbs = array(
    'S3',
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<p>
    Here you can find the examples of how to upload and download files. <b>Remember to include your credentials in the config file !!!</b>
<ul>
    <li><?= CHtml::link("Upload a File", array('/s3/createObject')) ?></li>
    <li><?= CHtml::link("Download File", array('/s3/getObject')) ?></li>

</ul>

</p>
